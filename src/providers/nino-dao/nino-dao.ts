
import { Injectable } from '@angular/core';
import { ClaseNino } from '../../Clases/Nino/clase.nino';
import * as firebase from 'firebase';
import { AngularFireDatabase } from '@angular/fire/database';
import { Observable } from 'rxjs';
import { UserDaoProvider } from '../user-dao/user-dao';


@Injectable()
export class NinoDaoProvider {


  items: Observable<any[]>;
  Allitems: any[];
  array: any[]=[];

  constructor(public afDB: AngularFireDatabase,public usdao: UserDaoProvider) {
    //this.items = afDB.list('nino').valueChanges();

  }


  saveCaracteristicas(nino:ClaseNino) {
    

      return new Promise<any>((resolve, reject) => {
        
        nino.key;
  
      this.afDB.object(`/nino/${ nino.key }`).update( nino )
       .then(
            (evt) => 
            {
              resolve( true );
            },
            (err) =>
            {
               reject(false);
            }
          );//fin then
      });
  
  }

  save(nino:ClaseNino){

    return new Promise<any>((resolve, reject) => {
      
      const newPostKey = firebase.database().ref().child('nino').push().key;
      nino.key=newPostKey;
      

    this.afDB.object(`/nino/${ newPostKey }`).update( nino )
     .then(
          (evt) => 
          {
            resolve( true );
          },
          (err) =>
          {
             reject(false);
          }
        );//fin then
    });

  }// fin save


  getAll(){
  
    return new Promise<any>((resolve,reject)=>{
      const requestRef = firebase.database().ref('nino');
      requestRef.orderByChild('escuelaKey')
      .equalTo(this.usdao.keyEscuela).limitToFirst(3)
      .once('value')
      .then((result) =>{
        this.Allitems=[];
              if(result){                      
                                          
                  result.forEach(element => {
                    this.Allitems.push(element.val());
                  });             
                  resolve(this.Allitems);
              }
              else 
              reject(null);
        });//end then
    });//end return promise*/
    
  }
  //obterner todos los ninos de un grupo
  getNinoByGroup(groupId: string){
    this.items=this.afDB.list('nino', ref=> ref.orderByChild('grupos/'+groupId).equalTo(true)).valueChanges();
     
       /* return new Promise<any>((resolve,reject)=>{
            const requestRef = firebase.database().ref('nino');
            requestRef.orderByChild('grupos/'+groupId)
            .equalTo(true)
            .once('value')
            .then((result) =>{
                    if(result){                      
                        this.array=[];                        
                        result.forEach(element => {
                          this.array.push(element.val());
                        });             
                        resolve(this.array);
                    }
                    else 
                    reject(null);
              });//end then
          });//end return promise*/
            
    }


    /* El metodo de arriba funciona, lo que hace es traer los Ids de los ninos que pertenezcan a cierto grupo
    ahora, en base a ese arreglo, hace falta recorrerlo y consultar la información de cada uno de los Ids
    */
    
    getNinosById(id: string){
    
      return new Promise<any>((resolve,reject)=>{     
        const requestRef = firebase.database().ref('nino/'+id);
        requestRef
        .once('value')
        .then((result) =>{
                if(result){                   
                  resolve(result.val());
                }
                else
                reject(false);
          });//end then
      });//end return promise
}
  
    
      

}
