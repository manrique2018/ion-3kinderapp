import { Injectable } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/database';
import * as firebase from 'firebase';



@Injectable()
export class SubirImagenProvider {

  porsentaje:number=0;
  text:string="cargado";
  error:string;
  constructor() {
 


  }

  presentLoading() {


  }



  cargarImagen_firebase(archivo:ArchivoSubir,ubicacion:string){

    let promesa = new Promise((resolve,reject)=>{

      let storeRef = firebase.storage().ref();
      let nombreArchivo :string = new Date().valueOf().toString();

      let upltask : firebase.storage.UploadTask = 
        storeRef.child(`img/actividades/${ubicacion}/${nombreArchivo}`) 
        .putString( archivo.img, 'base64', { contentType: 'image/jpeg' }  );

        /*let loader = this.loadingCtrl.create({
          content:this.text+' '+this.porsentaje
        });   
        loader.present();*/

      upltask.on(firebase.storage.TaskEvent.STATE_CHANGED,
            ()=>{
              //porsentaje de carga
              /*this.porsentaje= (upltask.snapshot.bytesTransferred / upltask.snapshot.totalBytes) * 100;
              loader.setContent(this.text+' '+this.porsentaje);*/
             
            },
            (err)=>{
              //hubo un error
              console.log(JSON.stringify(err)) ;
              this.error=JSON.stringify(err);
              //loader.dismiss(); 
              reject(null);      
            },
            ()=>{
              //se cargo bien
              upltask.snapshot.ref.getDownloadURL().then((downloadURL)=>{
                let url =downloadURL;  
                console.log(url); 
                //loader.dismiss();               
              resolve(url);
              })
    
        }
        
        )//uptask on end

    });
    return promesa;
  }


}

export interface ArchivoSubir{
  titulo:string;
  img:string;
  key?:string;
}