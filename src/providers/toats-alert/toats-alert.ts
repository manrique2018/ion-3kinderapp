import { Injectable } from '@angular/core';
import { Toast ,} from '@ionic-native/toast';
import {ToastController} from 'ionic-angular';
@Injectable()
export class ToatsAlertProvider {

  constructor(private toastCtrl: ToastController) {
    console.log('Hello ToatsAlertProvider Provider');
  }


  //recive el tiempo, si no lo manda pone 2seg
  toast_message_bottom(msg:string, time:number=2000){
    const toast= this.toastCtrl.create({
      message:msg,
      duration:time
    });
    toast.present();
  }
}
