import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ListaProfesPage } from './lista-profes';

@NgModule({
  declarations: [
    ListaProfesPage,
  ],
  imports: [
    IonicPageModule.forChild(ListaProfesPage),
  ],
})
export class ListaProfesPageModule {}
