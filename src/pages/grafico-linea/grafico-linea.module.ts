import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GraficoLineaPage } from './grafico-linea';

@NgModule({
  declarations: [
    GraficoLineaPage,
  ],
  imports: [
    IonicPageModule.forChild(GraficoLineaPage),
  ],
})
export class GraficoLineaPageModule {}
