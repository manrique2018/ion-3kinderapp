import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddEstudiantesPage } from './add-estudiantes';

@NgModule({
  declarations: [
    AddEstudiantesPage,
  ],
  imports: [
    IonicPageModule.forChild(AddEstudiantesPage),
  ],
})
export class AddEstudiantesPageModule {}