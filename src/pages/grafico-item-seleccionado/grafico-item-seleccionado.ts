import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ClaseAvtividad } from '../../Clases/Actividad/clase.actividad';
import { NinoDaoProvider } from '../../providers/nino-dao/nino-dao';
import { ClaseNino } from '../../Clases/Nino/clase.nino';

/**
 * Generated class for the GraficoItemSeleccionadoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
 
@IonicPage()
@Component({
  selector: 'page-grafico-item-seleccionado',
  templateUrl: 'grafico-item-seleccionado.html',
})
export class GraficoItemSeleccionadoPage {
  date:Date=null;
  note: string = "";
  actividad:ClaseAvtividad;
  nino:ClaseNino;
  estilo:string;

  hablar:boolean=false;
  leer:boolean=false;
  escribir:boolean=false;
  escuchar:boolean=false;  

  ritmoAp:number=1;
  plazAten:number=2;
  adecu:number=3;
  observaciones:string="";

  switchItem:string='actividad'
  constructor(public navCtrl: NavController, public navParams: NavParams,public ninoDao:NinoDaoProvider) {
    this.actividad=this.navParams.get('actividad');
    this.estilo=this.navParams.get('estilo');
   

    //this.nino=new ClaseNino();
    this.ninoDao.getNinosById(this.actividad.ninoKey).then((nino)=>{
      this.nino=nino;
     this. setUpcaracteristicas();

    });

    
  }

  setUpcaracteristicas(){

    this.hablar= this.nino.Hablar;
    this.escribir=this.nino.Escribir;
    this.leer=this.nino.Leer;
    this.escuchar= this.nino.Escuchar;
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad GraficoItemSeleccionadoPage');
  }

}
