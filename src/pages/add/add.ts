import { AdminDaoProvider } from './../../providers/admin-dao/admin-dao';
import { GrupoDaoProvider } from './../../providers/grupo-dao/grupo-dao';
import { UserDaoProvider } from './../../providers/user-dao/user-dao';
import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { ClaseGrupo } from '../../Clases/Grupo/clase.grupo';
import { AddIntegrantesPage } from '../add-integrantes/add-integrantes'; 
import { CompartirPage } from '../compartir/compartir'; 
import * as firebase from 'firebase';
import { Observable } from '../../../node_modules/rxjs';
import { ToatsAlertProvider } from '../../providers/toats-alert/toats-alert';

@Component({
  selector: 'page-add',
  templateUrl: 'add.html'
})
export class AddPage {
  items:Observable<any[]>;
  items2:Observable<any[]>;
  profes: any;
  yaExisten: any;
  shareValue: boolean;
  grupo:any;

  constructor(public toast:ToatsAlertProvider, public navCtrl: NavController, public userDao: UserDaoProvider, private adminDao: AdminDaoProvider, public navParams: NavParams, private grupoDao: GrupoDaoProvider) {
    this.grupo=this.navParams.get('grupo');
    this.obtenerProfesores();
    this.items = this.userDao.items;
    this.profes={};
  this.yaExisten={};
this.yaExisten[firebase.auth().currentUser.uid+""] = true;
  this.userDao.getGroupMembers(this.adminDao.keyAdmin, this.grupo.key);
    this.items2 = this.userDao.items2;
    this.prepararShare();
    this.prepararShare2();
    
  }

  enviarVinculo(){
    this.navCtrl.push(CompartirPage);
  }

  addIntegrantes(){
    this.navCtrl.push(AddIntegrantesPage);
  }

  obtenerProfesores(){
    this.userDao.getProfesoresFromUID(firebase.auth().currentUser.uid)
  }

  esProfesor(role){
    return role == "Profesor"
  }

  nombreCompleto(item){
    return  item.Nombre + " " +item.Apellido;
  }

  compartirGrupo(key, uid){

    if(this.yaExisten[uid+""] != null){
      this.toast.toast_message_bottom("Usuario ya agregado",3000);
    }else{
    this.grupoDao.compartirGrupo(key, this.grupo.key, uid)
    .then(()=>{
      this.toast.toast_message_bottom("Grupo compartido con exito",3000);
    });
  } 
  }

  prepararShare(){
    let index = 0;
    this.items.forEach(e=>{
      e.forEach( x=>{
        this.profes[x.key] = true; 
       
      })
    })
    console.log(this.profes);
  }


  prepararShare2(){
    let index = 0;
    this.items2.forEach(e=>{
      Object.keys(e[0]).forEach(key=>{
        this.yaExisten[key+""] = e[0][key+""];
      })

    })
    console.log(this.yaExisten);
  }

  botonCompartido(id){
    if(this.profes[id+""] != null && this.yaExisten[id+""] != null){
      return ["checkmark-circle", "verde"];
    }else{
      return ["add-circle","azul"];
    }
  }

  
}