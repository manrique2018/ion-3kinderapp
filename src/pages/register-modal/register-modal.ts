import { UserDaoProvider } from './../../providers/user-dao/user-dao';
import { Component } from '@angular/core';
import { NavController, NavParams, AlertController } from 'ionic-angular';
import { AngularFireAuth } from '@angular/fire/auth';
import { User } from '../../Clases/Usuario/clase.usuario';
import { AdminDaoProvider } from '../../providers/admin-dao/admin-dao';
import { LoginPage } from '../../pages/login/login';



@Component({
  selector: 'page-register-modal',
  templateUrl: 'register-modal.html',
})
export class RegisterModalPage {
  user = {} as User;
  roleType0 = false;
  roleType1 = false;
  roleType2 = false;
  correo = "" as string;
  type: number;
  profesor: boolean;
  institucion: string;


  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    private alertController: AlertController,
    private aFAuth: AngularFireAuth,
    private userDao: UserDaoProvider,
    private adminDao: AdminDaoProvider,
  ) {

    this.profesor = true;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RegisterModalPage');
  }

  closeModal(){
    this.navCtrl.pop();
  }

  selected(type){
    this.type = type;
    if(this.roleType0 == true || this.roleType1 == true || this.roleType2 == true){
      this.roleType0 = false;
      this.roleType1 = false;
      this.roleType2 = false;
      this.profesor = true;
    } else {
      if(type == 0){
        this.roleType1 = true;
        this.roleType2 = true;
        this.profesor = true;
      } else if(type == 1){
        this.roleType0 = true;
        this.roleType2 = true;
      } else {
        this.roleType1 = true;
        this.roleType0 = true;
        this.profesor = false;
        //aqui agregar link para profesor
      }
    }
  }

  cancelRegister(){
    this.navCtrl.pop();
  }

  registerKinder(){
    if(this.roleType0 == false && this.roleType1 == false && this.roleType2 == false){
      let alert = this.alertController.create({
        title: 'Tipo de cuenta sin seleccionar',
        subTitle: 'Debe seleccionar un tipo de cuenta.',
        buttons: ['Ok']
      });
      alert.present();
    }else{
      if(this.roleType0 == false){
        this.user.role = "Administrador";
      } else if(this.roleType1 == false){
        this.user.role = "Padre";
      } else {
        this.user.role = "Profesor";
      }
      let alert = this.alertController.create({
        title: 'Ingrese su contraseña',
        inputs: [
          {
            name: 'pass1',
            placeholder: 'Contraseña',
            type: 'password'
          },
          {
            name: 'pass2',
            placeholder: 'Repita la contrseña',
            type: 'password'
          }
        ],
        buttons: [
          {
            text: 'Cancelar',
            role: 'cancel',
            handler: data => {
              console.log('Cancel clicked');
            }
          },
          {
            text: 'Registrarse',
            handler: data => {
              if(data.pass1 === data.pass2){
                this.user.contrasena = data.pass1;
                this.user.fechaCreacion = new Date();
                this.register(this.user);
              } else {
                let alert = this.alertController.create({
                  title: 'Contraseñas diferentes',
                  subTitle: 'Las contraseñas no coinciden, por favor vuelva a intentarlo.',
                  buttons: ['Ok']
                });
                alert.present();
              }
            }
          }
        ]
      });
      alert.present();
    }
  }


  async register(user: User){
    try{     
      //falta corregir si el email esta malo. aqui croma y no hace nada

      switch(this.user.role){
          case "Administrador":
          const result = await this.aFAuth.auth.createUserWithEmailAndPassword(this.correo, user.contrasena);
          this.adminDao.save(result.user.uid).then((key)=>{
            this.user.escuelaKey=key;
            this.userDao.save(this.user, result.user.uid);
          });          
          this.navCtrl.push(LoginPage);
          break;

          case "Profesor":
          //this.profedorDao.save(result.user.uid).then((key)=>{
            //user.escuelaKey=key;
            this.adminDao.getAdminByID(this.institucion).then((res)=>{
              if(res.length != 0){
                this.aFAuth.auth.createUserWithEmailAndPassword(this.correo, user.contrasena)
                .then((result)=>{
                  this.user.escuelaKey = this.institucion;
                  this.userDao.saveProfesor(this.user, result.user.uid);
                });
              }else{
                alert("Admin no existe");
              }
            })      
            this.navCtrl.push(LoginPage);
          break;

      }      
    }
    catch(e){
      if(e.message == "The email address is already in use by another account."){
        let alert = this.alertController.create({
          title: 'El email ya ha sido utilizado',
          subTitle: 'El email ya ha sido usado por otra cuenta, digite uno diferente.',
          buttons: ['Ok']
        });
        alert.present();
      }
      else{
        console.error(e.message);
      }      
    }
  }

  saveProfile

}
