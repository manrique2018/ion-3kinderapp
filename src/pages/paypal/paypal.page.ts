import { Component } from '@angular/core';
import { PayPal, PayPalPayment, PayPalConfiguration } from '@ionic-native/paypal';

@Component({
	templateUrl: 'paypal.html'
})

export class PayPalPage {
	payment: PayPalPayment = new PayPalPayment('19.99', 'USD', 'PREMIUM', 'sale');
	currencies = ['EUR', 'USD'];
	payPalEnvironment: string = 'payPalEnvironmentSandbox';

    constructor(private payPal: PayPal) { }

	makePayment() {
		this.payPal.init({
			PayPalEnvironmentProduction: '',
			PayPalEnvironmentSandbox: 'AWdOWlPM1d37Z1H_DFKjfZwMK9DHzO3YXyFqlyPts35Av34tfIQ3i0NQDGrmvW_T8HpxNSI6vz_fodCZ'
		}).then(() => {
			this.payPal.prepareToRender(this.payPalEnvironment, new PayPalConfiguration({})).then(() => {
				this.payPal.renderSinglePaymentUI(this.payment).then((response) => {
					alert(`Successfully paid. Status = ${response.response.state}`);
					console.log(response);
				}, () => {
					console.error('Error or render dialog closed without being successful');
				});
			}, () => {
				console.error('Error in configuration');
			});
		}, () => {
			console.error('Error in initialization, maybe PayPal isn\'t supported or something else');
		});
	}
}