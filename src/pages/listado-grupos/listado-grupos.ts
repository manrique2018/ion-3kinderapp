import { AdminDaoProvider } from './../../providers/admin-dao/admin-dao';
import { Component, OnInit } from '@angular/core';
import { NavController, NavParams, ModalController, IonicPage } from 'ionic-angular';
import { GrupoDaoProvider } from '../../providers/grupo-dao/grupo-dao';
import { FormGrupoPage } from '../form-grupo/form-grupo'; 
import { Observable } from 'rxjs/Observable';
import { GrupNinosPage } from '../grup-ninos/grup-ninos';
import { AddPage } from '../add/add';
import { GrupoPage } from '../grupo/grupo';
import { grupo } from '../../providers/interfaces/interfaces';
import { ClaseGrupo } from '../../Clases/Grupo/clase.grupo';
import { UserDaoProvider } from '../../providers/user-dao/user-dao';
import { ToatsAlertProvider } from '../../providers/toats-alert/toats-alert';
import * as firebase from 'firebase';


@Component({
  selector: 'page-listado-grupos',
  templateUrl: 'listado-grupos.html',
})
export class ListadoGruposPage implements OnInit  {
  items: Observable<any[]>=this.grupoDao.items;
  array: any[]=this.grupoDao.array;
  constructor(public navCtrl: NavController, public navParams: NavParams, public grupoDao:GrupoDaoProvider,public usdao:UserDaoProvider,public toast:ToatsAlertProvider, public adminDao: AdminDaoProvider) {
    
    }

    ngOnInit() {

    if(this.usdao.keyEscuela != null) {

        this.grupoDao.getAll(this.usdao.keyEscuela);
        this.items= this.grupoDao.items; 

      }else{

        this.usdao.setUpUser().then((result)=>{
          if(result){
           this.grupoDao.getAll(this.usdao.keyEscuela);
            this.items= this.grupoDao.items; 
            
          }else{

            this.toast.toast_message_bottom("Se recomieda que vuelvas a iniciar sesion");

          }
        });
      }

    }

    irAct(grupo:ClaseGrupo){
      this.navCtrl.push(GrupoPage,{'grupo':grupo});
    }

    agregarGrupo(){
      this.navCtrl.push(FormGrupoPage);
    }

    add(grupo:ClaseGrupo){
      this.navCtrl.push(AddPage,{'grupo':grupo});
    }


    
}