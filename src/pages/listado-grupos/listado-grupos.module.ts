import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ListadoGruposPage } from './listado-grupos';

@NgModule({
  declarations: [
    ListadoGruposPage,
  ],
  imports: [
    IonicPageModule.forChild(ListadoGruposPage),
  ],
})
export class ListadoGruposPageModule {}