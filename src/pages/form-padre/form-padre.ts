import { PadreDaoProvider } from './../../providers/padre-dao/padre-dao';
import { ClasePadre } from './../../Clases/Padre/clase.padre';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Keyboard } from 'ionic-angular';
import { ToatsAlertProvider } from '../../providers/toats-alert/toats-alert';


@IonicPage()
@Component({
  selector: 'page-form-padre',
  templateUrl: 'form-padre.html',
})
export class FormPadrePage {

  padre: ClasePadre;

  constructor(public navCtrl: NavController, public navParams: NavParams, 
                private keyboard: Keyboard, private padreDao:PadreDaoProvider,
                private toast:ToatsAlertProvider) {
  this.padre = new ClasePadre('','','','','','','');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FormPadrePage');
  }

  registrar(){
    this.padreDao.save(this.padre).then((result)=>{
      let msg=" ";
    (result)? msg="Cargado correctamente.": msg="!!Valla, no se ha logrado cargar la información..";
    this.toast.toast_message_bottom(msg,3000);
  });
  }

}
