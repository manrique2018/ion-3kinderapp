import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FormPadrePage } from './form-padre';

@NgModule({
  declarations: [
    FormPadrePage,
  ],
  imports: [
    IonicPageModule.forChild(FormPadrePage),
  ],
})
export class FormPadrePageModule {}
