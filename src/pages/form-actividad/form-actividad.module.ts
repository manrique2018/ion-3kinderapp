import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FormActividadPage } from './form-actividad';

@NgModule({
  declarations: [
    FormActividadPage,
  ],
  imports: [
    IonicPageModule.forChild(FormActividadPage),
  ],
})
export class FormActividadPageModule {}
