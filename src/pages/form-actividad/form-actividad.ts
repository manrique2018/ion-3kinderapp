import { Component } from '@angular/core';
import { NavController, NavParams, Events, LoadingController } from 'ionic-angular';
import { ClaseAvtividad } from '../../Clases/Actividad/clase.actividad';
import { ActividadDaoProvider } from '../../providers/actividad-dao/actividad-dao';
import { ToatsAlertProvider } from '../../providers/toats-alert/toats-alert';
import { ClaseNino } from '../../Clases/Nino/clase.nino';
import { ImagePickerOptions, ImagePicker } from '@ionic-native/image-picker';
import { SubirImagenProvider,ArchivoSubir } from '../../providers/subir-imagen/subir-imagen';
import { UserDaoProvider } from '../../providers/user-dao/user-dao';



@Component({
  selector: 'page-form-actividad',
  templateUrl: 'form-actividad.html',
})
export class FormActividadPage {

  date:Date=null;
  note: string ;
  actividad:ClaseAvtividad;
  actividadEditar:ClaseAvtividad=null;
  enProceso:boolean=false;
  estilo:string;
  nino:ClaseNino;
  loader:any;
  imagenPreviw:string="";
  image64:string="";

  textoBotonEnviar:string;
  iconoBoton:string;


  constructor(public navCtrl: NavController,public events: Events,
     public acvtProvider:ActividadDaoProvider,
     private toast:ToatsAlertProvider,
     private navParam:NavParams,
     private imagePicker: ImagePicker,
     public imagDao:SubirImagenProvider,
     public loadingCtrl: LoadingController, public usdao:UserDaoProvider) {


    this.estilo=this.navParam.get('estilo');
    this.nino=this.navParam.get('nino');
    this.actividadEditar=this.navParam.get('actividadEditar');
    this.actividad=new ClaseAvtividad();

    if(this.actividadEditar != null){
      this.actividad=this.actividadEditar;
      this.imagenPreviw=this.actividad.urlImg; 
    }

   // si la actividad viene, es para editarla, la accion cambiaria al ejecutar el boton
   !this.actividadEditar? this.textoBotonEnviar="Registrar": this.textoBotonEnviar="Editar";
   !this.actividadEditar? this.iconoBoton="add": this.iconoBoton="md-create"; 

    events.subscribe('star-rating:changed', (starRating) => {
     this.actividad.valoracion=starRating;
     console.log(this.actividad.valoracion);
    });

    
    console.log(this.actividad); 

  }

// se valida si trae imagen o no.
  guardarActividad(){
    this.enProceso=true;

    // si la actividad no es editar
    if(!this.actividadEditar){
  
          this. loader = this.loadingCtrl.create({
            content:"Procesando.."
          });   
          this.loader.present();
      
          if(this.image64)
            this.subirImagen();
          else
          this.subirActividad();

        }// si la actividad es para editar
        else{
          this. loader = this.loadingCtrl.create({
            content:"Aplicando cambios.."
          });   
          this.loader.present();
          this.editarActividad();
        }
      
      
  }

  //carga la actividad a firebase
  subirActividad(){
    console.log(this.nino);
    this.actividad.ninoKey=this.nino.key; 
    this.actividad.escuela=this.usdao.keyEscuela;
    console.log(this.actividad);  
    this.acvtProvider.save(this.actividad,this.estilo).then((result=>{    
      let msj;

       !result? msj="Se ha producido un error al guardar":msj= "Guardado correctamente";
      this.toast.toast_message_bottom(msj); 

      if(result){
        this.actividad.inicializar();
        this.imagenPreviw="";
        this.image64="";
      }
        this.enProceso=false;
        this.loader.dismiss();
        this.navCtrl.pop();
        
    }));
  }

 

  editarActividad(){
    
        if(this.image64){
            let file:ArchivoSubir={
              titulo:"prueba",
              img:this.image64,
            };    
            this.imagDao.cargarImagen_firebase(file,this.estilo).then((url:string)=>{
              if( url!= null){
               this.actividad.urlImg=url;
                 this.editarFinalizar();   
              }else{
                this.enProceso=false;
                this.loader.dismiss();
                this.navCtrl.pop();
                this.toast.toast_message_bottom(this.imagDao.error,6000);
              }
            });

        }//fin if
         else
          this.editarFinalizar();
  }

  editarFinalizar(){
      
    this.acvtProvider.updateActividad(this.actividad,this.estilo).then((result=>{    
      let msj;

       !result? msj="Se ha producido un error al actualizar":msj= "Actualizado correctamente";
      this.toast.toast_message_bottom(msj,4000); 

      if(result){
        this.actividad=new ClaseAvtividad();
        this.actividad.inicializar();
        this.imagenPreviw="";
        this.image64="";
      }
        this.enProceso=false;
        this.loader.dismiss();
        this.navCtrl.pop();  
    }));

  }

      // permite abrir la galeria de imagenes
      openGallery(){
          let options:ImagePickerOptions={
            maximumImagesCount:1,
            outputType:1,
            quality:70,
          };
      
          this.imagePicker.getPictures(options).then((results) => {
            for (var i = 0; i < results.length; i++) {
              this.imagenPreviw  = 'data:image/jpeg;base64,' + results[i];
              this.image64  = results[i];
            }
          }, (err) => {
              this.toast.toast_message_bottom("No hemos logrado acceder a la galeria de imagenes...",3000);
           });
      }

    // permite subir una imagen a firebase y luego guarda la actividad con el url de la imagen
    subirImagen(){
          let file:ArchivoSubir={
            titulo:"prueba",
            img:this.image64,
          };    
          this.imagDao.cargarImagen_firebase(file,this.estilo).then((url:string)=>{
            if( url!= null){
             this.actividad.urlImg=url;
               this.subirActividad();   
            }else{
              this.enProceso=false;
              this.loader.dismiss();
              this.navCtrl.pop();
              this.toast.toast_message_bottom(this.imagDao.error,6000);
            }
          });
    }



}
