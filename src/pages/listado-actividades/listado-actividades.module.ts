import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ListadoActividadesPage } from './listado-actividades';

@NgModule({
  declarations: [
    ListadoActividadesPage,
  ],
  imports: [
    IonicPageModule.forChild(ListadoActividadesPage),
  ],
})
export class ListadoActividadesPageModule {}
