import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { ActividadDaoProvider } from '../../providers/actividad-dao/actividad-dao';
import { Observable } from 'rxjs';
import { ClaseNino } from '../../Clases/Nino/clase.nino';
import { ClaseAvtividad } from '../../Clases/Actividad/clase.actividad';
import { ActividadPage } from '../actividad/actividad';
import { FormActividadPage } from '../form-actividad/form-actividad';

@Component({
  selector: 'page-listado-actividades',
  templateUrl: 'listado-actividades.html',
})
export class ListadoActividadesPage {
  items: Observable<any[]>;
  nino:ClaseNino;
  estilo:string;
  constructor(public navCtrl: NavController, public navParams: NavParams, public actvDao:ActividadDaoProvider) {


    this.nino=this.navParams.get('nino');
    this.estilo=this.navParams.get('estilo');

    

    this.actvDao.getAll(this.estilo,this.nino.key);
    this.items=this.actvDao.items;  

  }

    verActividad(activ:ClaseAvtividad){
     this.navCtrl.push(ActividadPage,{'actividad':activ,'estilo':this.estilo});
    }
    
    
    agregarActividad(){
      this.navCtrl.push(FormActividadPage,{'estilo':this.estilo,'nino':this.nino,'actividadEditar':null});
    }
    

}
