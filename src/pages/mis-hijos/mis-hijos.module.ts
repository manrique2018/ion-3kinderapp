import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MisHijosPage } from './mis-hijos';

@NgModule({
  declarations: [
    MisHijosPage,
  ],
  imports: [
    IonicPageModule.forChild(MisHijosPage),
  ],
})
export class MisHijosPageModule {}
