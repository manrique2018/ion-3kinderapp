import { MisHijosPage } from './../mis-hijos/mis-hijos';
import { UtilitiesProvider } from './../../providers/utilities/utilities';
import { Component } from '@angular/core';
import { NavController, NavParams, Keyboard } from 'ionic-angular';
import { ClaseNino } from '../../Clases/Nino/clase.nino';
import { NinoDaoProvider } from '../../providers/nino-dao/nino-dao';
import { ImagePickerOptions, ImagePicker } from '@ionic-native/image-picker';
import { ToatsAlertProvider } from '../../providers/toats-alert/toats-alert';
import { TabsPage } from '../tabs/tabs';
import { UserDaoProvider } from '../../providers/user-dao/user-dao';


@Component({
  selector: 'page-form-nino',
  templateUrl: 'form-nino.html',
})
export class FormNinoPage {

  listo:boolean=false;
  nino:ClaseNino;
  checkNino:boolean=true;
  checkNina:boolean=false;
  puppies:boolean = true;
  info:string = 'basica';

  imagenPreviw:string="";
  image64:string="";

  keyGrupo:string;

  constructor(public navCtrl: NavController, public navParams: NavParams,
              public ninoDao:NinoDaoProvider, private imagePicker: ImagePicker, 
              public toast:ToatsAlertProvider, private utility: UtilitiesProvider,
              private keyboard: Keyboard,public usdao:UserDaoProvider ) {
    this.keyGrupo=this.navParams.get('keygrupo');
    this.nino=new ClaseNino('','','','','',false,false,false,false);
  }

  seleccionarSexoNina(){
    this.checkNina=true;
    this.checkNino=false;
  }


  seleccionarSexoNino(){
    this.checkNina=false;
    this.checkNino=true;
  }

  registrar(){
    this.nino.sexo =  this.checkNina ? 'F' : 'M';
    this.nino.escuelaKey=this.usdao.keyEscuela;
    this.nino.grupos[this.keyGrupo]=true;
    this.ninoDao.save(this.nino).then((result)=>{
        let msg=" ";
      (result)? msg="Cargado correctamente.": msg="!!Valla, no se ha logrado cargar la información..";
      this.toast.toast_message_bottom(msg,3000);
      this.ninoDao.getNinoByGroup(this.nino.escuelaKey);
      this.navCtrl.pop();
    });
  }

  nombreNino(): string{
    return this.nino.nombre == '' ? 'Ingresa un nombre.' : this.nino.nombre +" "+ this.nino.apellido1 +" "+ this.nino.apellido2;
  }

  _imagePicker(){

    let options:ImagePickerOptions={
      maximumImagesCount:1,
      outputType:1,
      quality:70
    };

    this.imagePicker.getPictures(options).then((results) => {
      for (var i = 0; i < results.length; i++) {
        this.imagenPreviw = 'data:image/jpeg;base64,' + results[i];
        this.image64 = results[i];
      }
    }, (err) => {
        this.toast.toast_message_bottom("Error al cargar la imagen..",3000);
     });
  }

  regresarRegistro(){
    this.navCtrl.push(MisHijosPage);
  }

}

