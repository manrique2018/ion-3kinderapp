import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddPadresPage } from './add-padres';

@NgModule({
  declarations: [
    AddPadresPage,
  ],
  imports: [
    IonicPageModule.forChild(AddPadresPage),
  ],
})
export class AddPadresPageModule {}
