import { Component } from '@angular/core';


//import { GruposPage } from '../grupos/grupos';
import { GraficosPage } from '../graficos/graficos';
import { ListadoGruposPage } from '../listado-grupos/listado-grupos';
import { MenuController } from 'ionic-angular';
import { UserDaoProvider } from '../../providers/user-dao/user-dao';
import { User } from '../../Clases/Usuario/clase.usuario';


@Component({
  selector: 'tabs-page',
  templateUrl: 'tabs.html'
})
export class TabsPage {
  role:string;
  pet: string = "kittens";
  tab1Root = ListadoGruposPage;
  tab2Root = GraficosPage;
  flag:boolean=true;
  user:User;
  constructor(private menu:MenuController, public usdao:UserDaoProvider) {
    this.usdao.setUpUser().then((result)=>{
      this.role= this.usdao.user.role;
      console.log(this.role);
    });
  }

  onChange($event){
    console.log($event.target.value);
    }

    openMenu(){
      this.menu.toggle();
    }
}
