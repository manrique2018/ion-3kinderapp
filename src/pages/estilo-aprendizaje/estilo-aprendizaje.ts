import { Component } from '@angular/core';
import {  NavController, NavParams, LoadingController } from 'ionic-angular';
import { ClaseNino } from '../../Clases/Nino/clase.nino';
import { ListadoActividadesPage } from '../listado-actividades/listado-actividades';
import { DomAdapter } from '@angular/platform-browser/src/dom/dom_adapter';
import { NinoDaoProvider } from '../../providers/nino-dao/nino-dao';
import { ToatsAlertProvider } from '../../providers/toats-alert/toats-alert';


@Component({
  selector: 'page-estilo-aprendizaje',
  templateUrl: 'estilo-aprendizaje.html',
})
export class EstiloAprendizajePage {
  nino:ClaseNino;
  visual:string="visual";
  auditivo:string="auditivo";
  kinestesico:string="kinestesico";
  switchItem: string = "Estilos";
  loader:any;
  hablar:boolean=false;
  leer:boolean=false;
  escribir:boolean=false;
  escuchar:boolean=false;  
  activarCaracteristicas:boolean=false;
  editar:boolean=true;
  edit:boolean=true;
 
  observaciones:string="";

  ritmoAp:number=1;
  plazAten:number=2;
  adecu:number=3;
  place: any;

  // se recive el nino por parametro 
  constructor(public navCtrl: NavController, public navParams: NavParams,public loadingCtrl: LoadingController,public ninoDao:NinoDaoProvider, public toast:ToatsAlertProvider) {
    this.nino=this.navParams.get('nino');
    this.setUpcaracteristicas();
    this.edit=true;
    console.log(this.nino);
  }

  setUpcaracteristicas(){

    this.hablar= this.nino.Hablar;
    this.escribir=this.nino.Escribir;
    this.leer=this.nino.Leer;
    this.escuchar= this.nino.Escuchar;
  }

  cambiarHabilidad(habilidad:any){

    switch(habilidad){
      case 'leer': this.leer=!this.leer; break;
      case 'escribir': this.escribir=!this.escribir;break;
      case 'escuchar': this.escuchar=!this.escuchar;break;
      case 'hablar': this.hablar=!this.hablar;break;
    }
        this.editar=false;
  }


  //recive el nombre de donde se almacenara la actividad ej: kinestesico
  agregarActividad(value:string){    
    console.log(value);
  }

  verActividades(estilo:string){
    this.navCtrl.push(ListadoActividadesPage,{'nino':this.nino,'estilo':estilo});
  }

  //caracteristicas de expresion
  editarCaract(){
    this.editar=true;
    this. loader = this.loadingCtrl.create({
      content:"Procesando.."
    });   
    this.loader.present();

    this.nino.Hablar=this.hablar;
    this.nino.Escribir=this.escribir;
    this.nino.Leer=this.leer;
    this.nino.Escuchar=this.escuchar;

    this.ninoDao.saveCaracteristicas(this.nino).then((result)=>{
      this.loader.dismiss();
      if(result){
        this.toast.toast_message_bottom("Editado correctamente..");
      }else{
        this.toast.toast_message_bottom("Se ha presentado un error al actualizar");
      }
    });
    
  }
// caracteristicas generales
  //caracteristicas de expresion
  editarCaractGenerales(){
    this.edit=true;
    this. loader = this.loadingCtrl.create({
      content:"Procesando.."
    });   
    this.loader.present();

    this.nino.ritmoAp=this.ritmoAp;
    this.nino.plazAten=this.plazAten;
    this.nino.adecu=this.adecu;
    this.nino.observaciones=this.observaciones;

    this.ninoDao.saveCaracteristicas(this.nino).then((result)=>{
      this.loader.dismiss();
      if(result){
        this.toast.toast_message_bottom("Editado correctamente..");
      }else{
        this.toast.toast_message_bottom("Se ha presentado un error al actualizar");
      }
    });
    
  }
 
  onChange(){
    this.edit=false;
  }

  keyup(){
    if(this.edit)
    this.edit=false;
  }
}



interface hablExpresionNino {
  titulo:string;
  Hablar:number;
  Escribir:number;
  Leer:number;
  Escuchar:number;
}


interface cartGeneralNino {
  titulo:string;
  ritmoAprend:string;
  retencion:string;
  adecuacion:string;
  observacion:string;

}
