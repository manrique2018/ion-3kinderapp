import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EstiloAprendizajePage } from './estilo-aprendizaje';

@NgModule({
  declarations: [
    EstiloAprendizajePage,
  ],
  imports: [
    IonicPageModule.forChild(EstiloAprendizajePage),
  ],
})
export class EstiloAprendizajePageModule {}
