import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ActividadDaoProvider } from '../../providers/actividad-dao/actividad-dao';
import { ClaseAvtividad } from '../../Clases/Actividad/clase.actividad';
import { ActividadPage } from '../actividad/actividad';
import { GraficoItemSeleccionadoPage } from '../grafico-item-seleccionado/grafico-item-seleccionado';

/**
 * Generated class for the GraficoDonaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-grafico-dona',
  templateUrl: 'grafico-dona.html',
})
export class GraficoDonaPage {

  public lineChartLabels:Array<any> = ['January', 'February', 'March', 'April', 'May', 'June', 'July'];
  public lineChartOptions:any = {
    responsive: true
  };
  public lineChartColors:Array<any> = [
    { // grey
      backgroundColor: 'rgba(47, 111, 214,0.50)', 
      borderColor: '#2f6fd6',
      pointBackgroundColor: '#2f6fd6',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(148,159,0,0.8)',
      pointRadius: 9,
      pointHitRadius: 10
    },
    { // dark grey
      backgroundColor: 'rgba(77,0,96,0.2)',
      borderColor: 'rgba(77,0,96,1)',
      pointBackgroundColor: 'rgba(77,0,96,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(77,0,96,1)'
    },
    { // grey
      backgroundColor: 'rgba(0,159,177,0.2)',
      borderColor: 'rgba(0,159,177,1)',
      pointBackgroundColor: 'rgba(0,159,177,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(0,159,177,0.8)'
    }
  ];
  public lineChartLegend:boolean = true;
  public lineChartType:string = 'line';
  
  
  // events
  public chartClicked(event,arr):void {
    console.log(event.active);
    let element= event.active;
    if (element.length > 0) {
        var value = element[0]._index;//index seleccionado
        this.actividad=this.actDao.arrayMovimiento[value];
        this.navCtrl.push(GraficoItemSeleccionadoPage,{'actividad':this.actividad,'estilo':'kinestesico'})
    }
  }
  
  public chartHovered(e:any):void {
    console.log(e);
  }
  array:number[]=[];
  
  public lineChartData:Array<any> = [
    {data: this.array, label: 'Kinestesico'}
  ];

  actividad:ClaseAvtividad;
  estilo:'kinestesico';
  terminado:boolean=false;

  constructor(public navCtrl: NavController, public navParams: NavParams,public actDao: ActividadDaoProvider) {
  
    this.actDao.getAllratingMovimiento().then((result)=>{

      result.forEach((element:ClaseAvtividad) => {
        let rate=element.valoracion;
       this.array.push(rate);
      });
      this.terminado=true;
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad GraficoDonaPage');
  }

}
