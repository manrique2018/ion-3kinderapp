import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GrupNinosPage } from './grup-ninos';

@NgModule({
  declarations: [
    GrupNinosPage,
  ],
  imports: [
    IonicPageModule.forChild(GrupNinosPage),
  ],
})
export class GrupNinosPageModule {}
