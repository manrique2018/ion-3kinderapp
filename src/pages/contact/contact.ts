import { Component } from '@angular/core';
import { NavController, Events } from 'ionic-angular';

@Component({
  selector: 'page-contact',
  templateUrl: 'contact.html'
})
export class ContactPage {

  date:Date=null;
  note: string = " ";
  constructor(public navCtrl: NavController,public events: Events) {
    events.subscribe('star-rating:changed', (starRating) => {console.log(starRating)});


  }

}
