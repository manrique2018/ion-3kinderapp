export class ClaseGrupo{
    nombre: string;
    urlImg:string;
    key:string;
    duenoKey:string;
    ninoKey:string;
    padreKey:string;
    escuelaKey:string;

    constructor() {
        this.inicializar();
    }

    inicializar(){
        this.nombre = "";
        this.urlImg = "";
        this.key="";
    }
}