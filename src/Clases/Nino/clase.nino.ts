

export class ClaseNino{
    nombre: string;
    apellido1: string;
    apellido2: string;
    edad:string;
    sexo:string;
    key:string;
    padreKey:string;
    urlImg:string;
    ambosPadres: boolean;
    hermanos: boolean;
    zonaUrbana: boolean;
    zonaRiesgo: boolean;
    escuelaKey:string;

    Hablar:boolean; 
    Escribir:boolean;
    Leer:boolean;
    Escuchar:boolean;  

    grupos : { [key:string]:boolean; } = {};
    ritmoAp:number;
    plazAten:number;
    adecu:number;
    observaciones:string;
    constructor(nombre: string, apellido1: string, apellido2: string
        , edad:string,    sexo:string, ambosPadres: boolean, hermanos: boolean,
        zonaUrbana: boolean, zonaRiesgo: boolean,) {
        this.nombre = nombre;
        this.apellido1 = apellido1;
        this.apellido2 = apellido2;
        this.edad=edad;
        this.sexo=sexo;
        this.ambosPadres = ambosPadres;
        this.hermanos = hermanos;
        this.zonaUrbana = zonaUrbana;
        this.zonaRiesgo = zonaRiesgo;

        this.Hablar=false;
        this. Escribir=false;
        this.Leer=false;
        this.Escuchar=false;

        this.ritmoAp=1;
       this. plazAten=1;
       this. adecu=1;
       this.observaciones="";
       
    }

}
