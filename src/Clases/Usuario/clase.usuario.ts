export class User{
    nombre: string;
    apellido: string;
    telefono: number;
    fechaCreacion: Date;
    role:string;
    key: string;
    contrasena: string;
    escuelaKey:string;

    constructor(nombre, apellido, telefono, role) {
        this.nombre = nombre;
        this.apellido = apellido;
        this.telefono = telefono;
        this.role = role;
        this.key = "";
        this.contrasena = "";
    }

}