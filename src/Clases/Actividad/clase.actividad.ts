




export class ClaseAvtividad{
    titulo: string;
    observacion: string;
    animo: string;
    descripcion:string;
    valoracion:number;
    key:string;
    ninoKey:string;
    fechaInicio:string;
    fechaFin:string;
    urlImg:string;
    escuela:string;


   
    constructor() {
this.inicializar();
    }


    inicializar(){
        this.titulo="";
        this.observacion="";
        this.animo="";
        this.descripcion="";
        this.valoracion=1;
        this.key="";
        this.ninoKey="";
        this.fechaInicio="";
        this.fechaFin="";
        this.urlImg="";
    }

}