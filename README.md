# Ionic 3, Angular 4 , Firebase. Prototype to help teachers and childs
###### This is an idea for help teachers and childs, creating groups with childs and allowing you to
###### create activities in three different areas, visual, auditive , and movement (kinesthetic) , with some variables related to each child. E.g.
###### Period of attention that he(she) have, skill to read.. 
###### You will rate the activiti result. then the data can be use in the future to get suggestions for other childs.

# Home

![Home](https://drive.google.com/uc?id=14bO_d-J6o-ZsjYOjkc5nSOlNOkseSOf3)
![Inside group](https://drive.google.com/uc?id=15XhgWAF6tb-cxrZUu0bE7C90ONtMzRFs)

# Inside a group

![Home](https://drive.google.com/uc?id=1p9adJrFbGaTftUd02NV8Ob34iP82WHLy)
![Inside group](https://drive.google.com/uc?id=1o-E85Nh0H7Crktox1T70i8R-WwW0fBQP)

# How it works
###### Inside a child you can use different variables (mentioned before "Period of attention that he(she) have, skill to read... and other") to use this information to help you while you are planning activities
###### and those information will be related to the activities, then when you browse to find suggestions, you can see the activities
###### and variables related to the child who recibed that activitie and the rating that it have. 

# Image of possible variables, this probbably will change!!
![Variables](https://drive.google.com/uc?id=1T5mvlE0VTU-r0J_7nPFlvLfIPWfy8KSh)
